const express = require('express');
const modelProductoNuevo = require('../../Models/refaccionaria/modelNuevoProducto');
const modelNuevoProducto = require('../../Models/refaccionaria/modelNuevoProducto');

let app = express();

app.post('/producto/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProducto = new modelProductoNuevo({
        nombreProducto: body.nombreProducto,
        marcaProducto: body.marcaProducto,
        presentacionProducto: body.presentacionProducto,
        contenidoProducto: body.contenidoProducto,
        costoProducto: body.costoProducto,
        proveedorProducto: body.proveedorProducto,
        cantidadIngresa: body.cantidadIngresa,
        statusProducto: body.statusProducto,
        descripcionProducto: body.descripcionProducto,
    });

    newSchemaProducto
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Error PA! No se guardaron los datos',
                        err
                    });
            }
        )
});

// buscar 
app.get('/obtener/datos/producto', async (req, res) => {
    const respuesta = await modelNuevoProducto.find();
        
    res.status(200).json({
        ok: true,
        respuesta
    });
});


// eliminar 
app.delete('/delete/producto/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelNuevoProducto.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar 
app.put('/update/producto/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelNuevoProducto.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

module.exports = app;