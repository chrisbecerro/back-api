const express = require('express');
const modelNuevoProveedor = require('../../Models/refaccionaria/modelNuevoProveedor');
let app = express();

app.post('/proveedor/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProveedor = new modelNuevoProveedor({
        nombreProveedor: body.nombreProveedor,
        empresaProveedor: body.empresaProveedor,
        domicilioProveedor: body.domicilioProveedor,
        telefonoProveedor: body.telefonoProveedor,
        emailProveedor: body.emailProveedor,
    });

    newSchemaProveedor
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Error PA! No se guardaron los datos',
                        err
                    });
            }
        )
});


// buscar
app.get('/obtener/datos/proveedor', async (req, res) => {
    const respuesta = await modelNuevoProveedor.find();


    res.status(200).json({
        ok: true,
        respuesta
    });
});

// eliminar
app.delete('/delete/proveedor/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelNuevoProveedor.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar
app.put('/update/proveedor/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelNuevoProveedor.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

module.exports = app;