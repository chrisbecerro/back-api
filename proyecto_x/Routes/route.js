const express = require('express');
const app = express();

app.use(require('./test'));
app.use(require('./productoNuevo/routeNuevoProducto'));
app.use(require('./clienteNuevo/routeNuevoCliente'));
app.use(require('./personalNuevo/routeNuevoPersonal'));
app.use(require('./proveedorNuevo/routeNuevoProveedor'));



module.exports = app;