const express = require('express');
const modelNuevoCliente = require('../../Models/refaccionaria/modelNuevoCliente');

let app = express();

app.post('/cliente/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaCliente = new modelNuevoCliente({
        nombreCliente: body.nombreCliente,
        apellidoCliente: body.apellidoCliente,
        sexoCliente: body.sexoCliente,
        edadCliente: body.edadCliente,
        domicilioCliente: body.domicilioCliente,
    });

    newSchemaCliente
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Error PA! No se guardaron los datos',
                        err
                    });
            }
        )
});

// buscar Cliente
app.get('/obtener/datos/cliente', async (req, res) => {
    const respuesta = await modelNuevoCliente.find();


    res.status(200).json({
        ok: true,
        respuesta
    });
});


// eliminar Cliente
app.delete('/delete/cliente/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelNuevoCliente.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar Cliente
app.put('/update/cliente/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelNuevoCliente.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});
module.exports = app;