const express = require('express');
const modelNuevoPersonal = require('../../Models/refaccionaria/modelNuevoPersonal');

let app = express();

app.post('/personal/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaPersonal = new modelNuevoPersonal({
        nombrePersonal: body.nombrePersonal,
        apellidoPersonal: body.apellidoPersonal,
        sexoPersonal: body.sexoPersonal,
        edadPersonal: body.edadPersonal,
        sueldoPersonal: body.sueldoPersonal,
    });

    newSchemaPersonal
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Error PA! No se guardaron los datos',
                        err
                    });
            }
        )
});


// buscar personal

app.get('/obtener/datos/personal', async (req, res) => {
    const respuesta = await modelNuevoPersonal.find();


    res.status(200).json({
        ok: true,
        respuesta
    });
});


// eliminar personal
app.delete('/delete/personal/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelNuevoPersonal.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar
app.put('/update/personal/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelNuevoPersonal.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

module.exports = app;