const mongoose = require('mongoose');
const app = require('../../Server');

let Schema = mongoose.Schema;

let nuevoClienteRefaccionaria = new Schema({
    nombreCliente: {type: String},
    apellidoCliente: {type: String},
    sexoCliente: {type: String},
    edadCliente : {type: Number},
    domicilioCliente: {type: String},
});

module.exports = mongoose.model('nuevoCliente', nuevoClienteRefaccionaria);