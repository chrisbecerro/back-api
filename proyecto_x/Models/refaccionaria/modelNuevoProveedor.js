const mongoose = require('mongoose');
const app = require('../../Server');

let Schema = mongoose.Schema;

let nuevoProveedorRefaccionaria = new Schema({
    nombreProveedor: {type: String},
    empresaProveedor: {type: String},
    domicilioProveedor: {type: String},
    telefonoProveedor : {type: Number},
    emailProveedor: {type: String},
});

module.exports = mongoose.model('nuevoProveedor', nuevoProveedorRefaccionaria);