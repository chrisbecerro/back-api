const mongoose = require('mongoose');
const app = require('../../Server');

let Schema = mongoose.Schema;

let nuevoProductoRefaccionaria = new Schema({
    nombreProducto: {type: String},
    marcaProducto: {type: String},
    presentacionProducto: {type: String},
    contenidoProducto: {type: String},
    costoProducto: {type: Number},
    proveedorProducto: {type: String},
    cantidadIngresa: {type: Number},
    statusProducto: {type: Boolean},
    descripcionProducto: {type: String},
});

module.exports = mongoose.model('nuevoProducto', nuevoProductoRefaccionaria);