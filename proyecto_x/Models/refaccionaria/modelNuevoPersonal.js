const mongoose = require('mongoose');
const app = require('../../Server');

let Schema = mongoose.Schema;

let nuevoPersonalRefaccionaria = new Schema({
    nombrePersonal: {type: String},
    apellidoPersonal: {type: String},
    sexoPersonal: {type: String},
    edadPersonal : {type: Number},
    sueldoPersonal: {type: Number}
});

module.exports = mongoose.model('nuevoPersonal', nuevoPersonalRefaccionaria);