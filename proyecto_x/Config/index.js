//conexion a la base de datos
// console.log('Nodemon ejecutandose correctamente!');
'use strict'

const mongoose = require('mongoose');
const app = require('../Server/index');
const port = 3900;

//Generar promesa global
mongoose.Promise = global.Promise;

// Aqui hacemos la conexion a la db
mongoose.connect('mongodb://localhost:27017/refaccionaria', {useNewUrlParser: true})
    .then(() => {
        console.log('Base de datos corriendo de uña PA');

        //Escuchar el puerto del server
        app.listen(port, () => {
            console.log(`Server corriendo en el puerto: ${port}`);
        });
    });



